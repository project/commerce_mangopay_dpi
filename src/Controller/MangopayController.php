<?php

namespace Drupal\commerce_mangopay_dpi\Controller;

use DateTime;
use DateTimeZone;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * MANGOPAY controller
 */
class MangopayController implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new MangopayController object.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('messenger'));
  }

  /**
   * Callback method which pre registeres user's card
   * and creates user - wallet combo if it doesn't exist.
   *
   * @param PaymentGatewayInterface $commerce_payment_gateway
   * @param Request $request
   * @return void
   */
  public function preRegisterCard(PaymentGatewayInterface $commerce_payment_gateway, Request $request) {
    // Capture user details passed in the request
    $currency_code = $request->get('currency_code');
    if (empty($currency_code)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Currency code is required'], 400);
      $response->send();
      exit;
    }

    /** @var \Drupal\commerce_price\Entity\Currency $currency */
    $currency = \Drupal::entityTypeManager()->getStorage('commerce_currency')->load($currency_code);
    if (empty($currency)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Currency code is invalid'], 400);
      $response->send();
      exit;
    }

    $first_name = $request->get('first_name');
    if (empty($first_name)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'First name is required'], 400);
      $response->send();
      exit;
    }

    $last_name = $request->get('last_name');
    if (empty($last_name)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Last name is required'], 400);
      $response->send();
      exit;
    }

    $email = $request->get('email');
    if (empty($email)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Email is required'], 400);
      $response->send();
      exit;
    }

    $dob = $request->get('dob');
    if (empty($dob)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Date of birth is required'], 400);
      $response->send();
      exit;
    }
    $dob = new DateTime($dob . ' 00:00:00', new DateTimeZone('UTC'));

    $address_line1 = $request->get('address_line1');
    $address_line2 = $request->get('address_line2');
    $city = $request->get('city');
    $postal_code = $request->get('postal_code');
    $region = $request->get('region');
    $country = $request->get('country');
    if (empty($country)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Country is required'], 400);
      $response->send();
      exit;
    }

    $nationality = $request->get('nationality');
    if (empty($nationality)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Nationality is required'], 400);
      $response->send();
      exit;
    }

    $card_type = $request->get('card_type');
    if (empty($card_type)) {
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Card type is required'], 400);
      $response->send();
      exit;
    }

    /** @var \Drupal\commerce_mangopay_dpi\Plugin\Commerce\PaymentGateway\MangopayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $commerce_payment_gateway->getPlugin();
    $account = \Drupal::currentUser();
    $user = NULL;
    $mangopay_user = NULL;

    // Load user if authenticated.
    if ($account->isAuthenticated()) {
      $user = User::load($account->id());
    }

    // Check if the currently logged in user has already Remote Id set.
    // If yes, try to fetch the MANGOPAY user from the API.
    if ($user) {
      /** @var \Drupal\commerce\Plugin\Field\FieldType\RemoteIdFieldItemListInterface $remote_ids */
      $remote_ids = $user->get('commerce_remote_id');
      $mangopay_remote_id = $remote_ids->getByProvider($commerce_payment_gateway->id() . '|' . $payment_gateway_plugin->getMode());
      if (!empty($mangopay_remote_id)) {
        try {
          $mangopay_user = $payment_gateway_plugin->getUser($mangopay_remote_id);
        } catch(\Exception $e) {
          \Drupal::logger('commerce_mangopay_dpi')->notice(sprintf('Unable to retrieve MANGOPAY user %s while registering a card: %s: %s', $mangopay_remote_id, $e->getCode(), $e->getMessage()));
        }
      }
    }

    // IF no MANGOPAY user retrieved, try to create it.
    if (!$mangopay_user) {
      // Create user for payment if there is no Remote Id already stored in Drupal.
      try {
        $mangopay_user = $payment_gateway_plugin->createNaturalUser($first_name, $last_name, $email, $dob->format('U'), $nationality, $country, $address_line1, $address_line2, $city, $postal_code, $region, '', '', $payment_gateway_plugin->getTag());
      } catch(\Exception $e) {
        \Drupal::logger('commerce_mangopay_dpi')->error(sprintf('Unable to create MANGOPAY user while registering a card: %s: %s', $e->getCode(), $e->getMessage()));
        $response = new JsonResponse([
          'status' => 'Critical',
          'message' => 'Unable to create MANGOPAY user'], 500);
        $response->send();
        exit;
      }

      // Set MANGOPAY User Id on the user object if the account is logged in.
      if ($user) {
        /** @var \Drupal\commerce\Plugin\Field\FieldType\RemoteIdFieldItemListInterface $remote_ids */
        $remote_ids = $user->get('commerce_remote_id');
        $remote_ids->setByProvider($commerce_payment_gateway->id() . '|' . $payment_gateway_plugin->getMode(), $mangopay_user->Id);
        $user->save();
      }
    }

    // Check if user already has an active wallet for Drupal Commerce with specified currency.
    // If yes, use it. Otherwise, create a new one.
    $mangopay_wallet = NULL;
    try {
      $wallets = $payment_gateway_plugin->getWallets($mangopay_user->Id);
      foreach($wallets as $wallet) {
        if ($wallet->Tag == $payment_gateway_plugin->getTag()
          && $wallet->Currency == $currency_code) {
          $mangopay_wallet = $wallet;
          continue;
        }
      }
    } catch(\Exception $e) {
      \Drupal::logger('commerce_mangopay_dpi')->notice(sprintf('Unable to retrieve MANGOPAY wallets for user %s', $mangopay_user->Id));
    }

    // If yes, use it, otherwise create a new one.
    if (!$mangopay_wallet) {
      try {
        $mangopay_wallet = $payment_gateway_plugin->createWallet($mangopay_user->Id, $currency_code, sprintf('%s wallet', $currency->getName()), $payment_gateway_plugin->getTag());
      } catch (\Exception $e) {
        \Drupal::logger('commerce_mangopay_dpi')
          ->error(sprintf('Unable to create MANGOPAY wallet for user %s while registering a card: %s: %s', $mangopay_user->Id, $e->getCode(), $e->getMessage()));
        $response = new JsonResponse([
          'status' => 'Critical',
          'message' => 'Unable to create MANGOPAY wallet'
        ], 500);
        $response->send();
        exit;
      }
    }

    // Initiate card registration
    try {
      $card_register = $payment_gateway_plugin->createCardRegistration($mangopay_user->Id, $currency_code, $card_type, $payment_gateway_plugin->getTag());
    } catch(\Exception $e) {
      \Drupal::logger('commerce_mangopay_dpi')->error(sprintf('Unable to register card for user %s and wallet %s: %s: %s', $mangopay_user->Id, $mangopay_wallet->Id, $e->getCode(), $e->getMessage()));
      $response = new JsonResponse([
        'status' => 'Critical',
        'message' => 'Unable to register card'], 500);
      $response->send();
      exit;
    }

    // TODO: Handle errors better. Maybe some are recoverable? - https://docs.mangopay.com/guide/errors

    // Send response to the browser
    $response = new JsonResponse([
        'userId' => $mangopay_user->Id,
        'walletId' => $mangopay_wallet->Id,
        'cardRegistrationURL' => $card_register->CardRegistrationURL,
        'preregistrationData' => $card_register->PreregistrationData,
        'cardRegistrationId' => $card_register->Id,
        'cardType' => $card_register->CardType,
        'accessKey' => $card_register->AccessKey
      ]);
    $response->send();
    exit;
  }

  /**
   * Callback method for secure mode (3DS) results.
   *
   * @param Request $request
   * @param RouteMatchInterface $route_match
   * @return void
   */
  public function processSecureMode(Request $request, RouteMatchInterface $route_match) {

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $commerce_payment = $route_match->getParameter('commerce_payment');
    $step_id = $route_match->getParameter('step');

    // Validate payment state. We allow only NEW payments here.
    $state = $commerce_payment->getState()->value;
    if (!in_array($state, ['new'])) {
      throw new \InvalidArgumentException(sprintf('The provided payment is in an invalid state ("%s").', $state));
    }

    // Validate payment method
    $payment_method = $commerce_payment->getPaymentMethod();
    if (empty($payment_method)) {
      throw new \InvalidArgumentException('The provided payment has no payment method referenced.');
    }
    if ($payment_method->isExpired()) {
      throw new HardDeclineException('The provided payment method has expired');
    }

    // Validate Remote ID exists
    if (empty($commerce_payment->getRemoteId())) {
      throw new \InvalidArgumentException('Payment missing remote Id');
    }

    $payment_gateway = $commerce_payment->getPaymentGateway();
    /** @var \Drupal\commerce_mangopay_dpi\Plugin\Commerce\PaymentGateway\MangopayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    

    // Get Remote PayIn object and check its status.
    try {
      $payin = $payment_gateway_plugin->getPayIn($commerce_payment->getRemoteId());
    } catch(\Exception $e) {
      \Drupal::logger('commerce_mangopay_dpi')->error(sprintf('Unknown critical error occurred while fetching pay in object %s', $commerce_payment->getRemoteId()));
      throw new \InvalidArgumentException('MANGOPAY Pay In object not found');
    }

    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlow $checkout_flow */
    $checkout_flow = $order->get('checkout_flow')->entity;
    /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesInterface $checkout_flow_plugin */
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    $error_step_id = $checkout_flow_plugin->getPane('payment_information')->getStepId();

    // Update payment object and redirect accordingly.
    switch($payin->Status) {
      case \MangoPay\PayInStatus::Succeeded:
        // Mark payment's state as completed.
        $commerce_payment->setState('completed');
        $commerce_payment->save();

        $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);
      break;

      case \MangoPay\PayInStatus::Failed:
        // TODO: Display more descriptive messages here for the transaction errors: https://docs.mangopay.com/guide/errors
        $commerce_payment->getPaymentMethod()->setReusable(false)->save();
        $commerce_payment->getOrder()->set('payment_method', NULL)->save();
        $commerce_payment->delete();

        \Drupal::logger('commerce_mangopay_dpi')->warning(sprintf('Pay In Failure (3DS): %s: %s', $payin->ResultCode, $payin->ResultMessage));
        $message = $this->t('We encountered an unexpected error processing your payment method. Please try again later.');
        $this->messenger->addError($message);
        $redirect_step_id = $error_step_id;
      break;

      default:
        $commerce_payment->getPaymentMethod()->setReusable(false)->save();
        $commerce_payment->getOrder()->set('payment_method', NULL)->save();
        $commerce_payment->delete();

        \Drupal::logger('commerce_mangopay_dpi')->error(sprintf('Pay In Error (3DS): %s: %s', $payin->ResultCode, $payin->ResultMessage));
        $message = $this->t('We encountered an unexpected error processing your payment method. Please try again later.');
        $this->messenger->addError($message);
        $redirect_step_id = $error_step_id;

      break;
    }

    $checkout_flow_plugin->redirectToStep($redirect_step_id);
  }
}