<?php

namespace Drupal\commerce_mangopay_dpi\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;

/**
 * Provides the Off-site Secure Mode (3DS) redirect
 *
 * @CommercePaymentGateway(
 *   id = "commerce_mangopay_dpi",
 *   label = "MANGOPAY Direct PayIn",
 *   display_label = "MANGOPAY Direct PayIn",
 *   requires_billing_information = FALSE,
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_mangopay_dpi\PluginForm\Onsite\PaymentMethodAddForm",
 *   },
 *   modes = {"sandbox" = "Sandbox", "production" = "Production"},
 *   payment_method_types = {"commerce_mangopay_dpi_credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 * )
 */
class Mangopay extends OnsitePaymentGatewayBase implements MangopayInterface {
  const STANDARD_TAG = 'drupal commerce';

  /**
   * @var \MangoPay\MangoPayApi
   */
  protected $api;

  /**
   *
   * @return \MangoPay\MangoPayApi
   */
  public function getApi() {
    return $this->api;
  }

  /**
   * @return mixed
   */
  public function getTag() {
    return $this->getConfiguration()['tag'];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    // Construct MANGOPAY Api object
    $mode = $this->getConfiguration()['mode'];
    $client_id = $this->getConfiguration()['client_id'];
    $client_pass = $this->getConfiguration()['client_pass'];
    switch($mode) {
      case 'production':
        $base_url = 'https://api.mangopay.com';
        break;
      default:
        $base_url = 'https://api.sandbox.mangopay.com';
        break;
    }

    // Create instance of MangoPayApi SDK
    $this->api = new \MangoPay\MangoPayApi();
    $this->api->Config->BaseUrl = $base_url;
    $this->api->Config->ClientId = $client_id;
    $this->api->Config->ClientPassword = $client_pass;
    $this->api->Config->TemporaryFolder = file_directory_temp();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'client_id' => '',
      'client_pass' => '',
      'simple_kyc' => FALSE,
      'tag' => 'commerce_mangopay_dpi',
      '3ds_threshold' => '',
      '3ds_countries' => array(),
      '3ds_whitelist' => array(),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // The PaymentInformation pane uses payment method labels
    // for on-site gateways, the display label is unused.
    $form['display_label']['#access'] = FALSE;

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Id'),
      '#description' => $this->t('Please enter your MANGOPAY client id applicable to the environment (mode) you\'re using.'),
      '#default_value' => $this->configuration['client_id'],
      '#required' => TRUE,
    ];

    $form['client_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Client Password'),
      '#description' => $this->t('Please enter your MANGOPAY client password applicable to the environment (mode) you\'re using.'),
      '#default_value' => $this->configuration['client_pass']
    ];

    $form['simple_kyc'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Simplified KYC'),
      '#description' => $this->t('Simplified Know Your Customer information gathering for marketplaces.'),
      '#default_value' => $this->configuration['simple_kyc'],
    ];

    $form['tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tag'),
      '#description' => $this->t('Standard tag to mark all MANGOPAY resources with. Used to identify resources \'owned\' by this payment gateway. Please note that once set, it\'s not recommended to change this value.'),
      '#default_value' => $this->configuration['tag'],
      '#required' => TRUE,
    ];

    $form['3ds_threshold'] = [
      '#type' => 'number',
      '#step' => 0.01,
      '#title' => $this->t('Default secure mode threshold'),
      '#description' => $this->t('Transaction value over which Secure Mode (3D Secure) will be forced. Please note that maximum value for MANGOPAY is 50 Euro, so 3D Secure will always be forced for transactions above 50 Euro. In order to raise this limit, please contact your MANGOPAY representative.'),
      '#default_value' => $this->configuration['3ds_threshold'],
      '#required' => FALSE,
    ];

    $form['3ds_countries'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Secure mode threshold per country'),
      '#description' => $this->t('You can override Secure Mode threshold per country. Please use ISO alpha-2 codes and define threshold for given country code after the pipe (i.e. US|20, GB|50, ES|100), one entry per line. Please note that 3D Secure will always be forced for transactions above 50 Euro, regardless of this setting. In order to raise this limit, please contact your MANGOPAY representative.'),
      '#default_value' => self::valuesString($this->configuration['3ds_countries'], TRUE),
      '#required' => FALSE,
    ];

    $form['3ds_whitelist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Secure mode whitelist'),
      '#description' => $this->t('You can whitelist specific countries from Secure Mode requirement set in the threshold fields above. Please use ISO alpha-2 codes (i.e. US, GB, ES), one code per line. Please note that 3D Secure will always be forced for transactions above 50 Euro, regardless of this setting. In order to raise this limit, please contact your MANGOPAY representative.'),
      '#default_value' => self::valuesString($this->configuration['3ds_whitelist']),
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // Split 3ds_whitelist

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['client_id'] = $values['client_id'];
      if (!empty($values['client_pass'])) {
        $this->configuration['client_pass'] = $values['client_pass'];
      }
      $this->configuration['simple_kyc'] = $values['simple_kyc'];
      $this->configuration['tag'] = $values['tag'];
      $this->configuration['3ds_threshold'] = $values['3ds_threshold'];
      $this->configuration['3ds_countries'] = self::extractValues($values['3ds_countries'], TRUE);
      $this->configuration['3ds_whitelist'] = self::extractValues($values['3ds_whitelist']);
    }
  }

  /**
   * @param $values
   * @return string
   */
  private static function valuesString($values, $merge_tuples = FALSE) {
    if (!empty($values)) {
      if (!$merge_tuples) {
        return implode("\n", $values);
      }

      $tuples = [];
      foreach($values as $key => $value) {
        $tuples[] = $key . '|'  . $value;
      }
      return implode("\n", $tuples);
    }
  }

  /**
   * @param $string
   * @param $create_tuples
   * @return array
   */
  private static function extractValues($string, $create_tuples = FALSE) {
    $list = explode("\n", $string);
    $list = array_map('trim', $list);
    $list = array_map('strtoupper', $list);
    $list = array_filter($list, 'strlen');

    if ($create_tuples) {
      $tuples = [];
      foreach($list as $entry) {
        $tuple = explode('|', $entry);
        if (count($tuple) > 1) {
          $tuples[$tuple[0]] = $tuple[1];
        }
        else {
          $tuples[$tuple[0]] = NULL;
        }
      }
      return $tuples;
    }

    return $list;
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      'card_type', 'card_alias', 'card_id', 'user_id', 'wallet_id', 'expiration', 'cardholder_country', 'cardholder_name', 'dob', 'nationality'
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // Set remote User Id on the user object.
    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated()) {
      $this->setRemoteCustomerId($owner, $payment_details['user_id']);
      $owner->save();
    }

    // Set relevant details on payment method object.
    $payment_method->user_id = $payment_details['user_id'];
    $payment_method->wallet_id = $payment_details['wallet_id'];
    $payment_method->card_type = $payment_details['card_type'];
    $payment_method->card_number = $payment_details['card_alias'];
    $payment_method->card_exp_month = $payment_details['expiration']['month'];
    $payment_method->card_exp_year = $payment_details['expiration']['year'];
    $payment_method->currency_code = $payment_details['currency_code'];
    $payment_method->name = $payment_details['cardholder_name'];
    $payment_method->country = $payment_details['cardholder_country'];
    $payment_method->nationality = $payment_details['nationality'];
    $payment_method->dob = $payment_details['dob'];
    $payment_method->setRemoteId($payment_details['card_id']);
    $payment_method->setExpiresTime(CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']));
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();

    // TODO: Instruct MANGOPAY API to remove the credit card? Is this possible?
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // Payment must be saved on this step in order to generate unique ID
    $payment->save();

    // Validate payment state. We allow only NEW payments here.
    $state = $payment->getState()->value;
    if (!in_array($state, ['new'])) {
      throw new PaymentGatewayException('Payment has been already processed.');
    }

    // Validate payment method
    $payment_method = $payment->getPaymentMethod();
    if (empty($payment_method)) {
      throw new PaymentGatewayException('No payment method specified.');
    }

    if ($payment_method->isExpired()) {
      throw new HardDeclineException('The provided payment method has expired.');
    }

    $route_match = \Drupal::routeMatch();
    $step_id = $route_match->getParameter('step');
    if (empty($step_id)) {
      throw new PaymentGatewayException('Could not determine current step.');
    }

    // Fetch required information from our payment and payment_method objects
    $order = $payment->getOrder();
    $amount = doubleval($payment->getAmount()->getNumber()) * 100;
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $card_id = $payment_method->getRemoteId();
    $user_id = $payment_method->user_id->value;
    $wallet_id = $payment_method->wallet_id->value;
    $payment_gateway_configuration = $this->getConfiguration();

    // Determine if we should force secure mode based on the default threshold.
    $secure_mode = 'DEFAULT';
    if (!empty($payment_gateway_configuration['3ds_threshold'])
      && $payment->getAmount()->getNumber()
      >= $payment_gateway_configuration['3ds_threshold']) {
      $secure_mode = 'FORCE';
    }

    // Determine if country overrides 3D Secure threshold requirements.
    if ($payment_method->hasField('country')) {
      $billing_country_code = $payment_method->get('country')->getString();

      // Per country override
      if (!empty($payment_gateway_configuration['3ds_countries'][$billing_country_code])) {
        $secure_mode = 'DEFAULT';
        if ($payment->getAmount()->getNumber()
          >= $payment_gateway_configuration['3ds_countries'][$billing_country_code]) {
          $secure_mode = 'FORCE';
        }
      }

      // Per country whitelist
      if (in_array($billing_country_code, $payment_gateway_configuration['3ds_whitelist'])) {
        $secure_mode = 'DEFAULT';
      }
    }
    
    try {
      // Allow other modules to alter to payin before it is executed
      $payin_arguments = [
        'user_id' => $user_id,
        'wallet_id' => $wallet_id,
        'card_id' => $card_id,
        'amount' => $amount,
        'currency_code' => $currency_code,
        'secure_mode_return_url' => Url::fromRoute('commerce_mangopay_dpi.process_secure_mode', ['commerce_order' => $order->id(), 'commerce_payment' => $payment->id(), 'step' => $step_id], ['absolute' => TRUE])->toString(),
        'secure_mode' => $secure_mode
      ];
      \Drupal::moduleHandler()->alter('commerce_mangopay_dpi_pay_in', $payin_arguments);

      $payin = $this->createDirectPayIn(
        $payin_arguments['user_id'], 
        $payin_arguments['wallet_id'], 
        $payin_arguments['card_id'], 
        $payin_arguments['amount'], 
        $payin_arguments['currency_code'], 
        $payin_arguments['secure_mode_return_url'], 
        $payin_arguments['secure_mode']);

      // Allow other modules to react to payin.
      \Drupal::moduleHandler()->invokeAll('commerce_mangopay_dpi_pay_in', [$payin, $payment]);
    } catch(\Exception $e) {
      \Drupal::logger('commerce_mangopay_dpi')->error(sprintf('Unable to create Direct PayIn for card %s: %s: %s', $payin_arguments['card_id'], $e->getCode(), $e->getMessage()));
      throw new PaymentGatewayException('Payment has not been processed correctly. Please try again with a different card.');
    }

    switch($payin->Status) {
      case \MangoPay\PayInStatus::Failed:
        $payment->getPaymentMethod()->setReusable(false)->save();
        $payment->getOrder()->set('payment_method', NULL)->save();
        $payment->delete();

        // TODO: Display more descriptive messages here for the transaction errors: https://docs.mangopay.com/guide/errors
        \Drupal::logger('commerce_mangopay_dpi')->warning(sprintf('Pay In Failure: %s: %s', $payin->ResultCode, $payin->ResultMessage));
        throw new HardDeclineException('Payment has not been processed correctly. Please try again with a different card.');

      // 3DS / Secure Mode, needs further processing.
      case \MangoPay\PayInStatus::Created:
        $payment->setRemoteId($payin->Id);
        $payment->save();

        if ($payin->ExecutionDetails->SecureModeNeeded && !empty($payin->ExecutionDetails->SecureModeRedirectURL)) {
          // Redirect to 3D Secure, please in order to handle customer auth.
          // TODO: Do we need to validate the URL?
          throw new NeedsRedirectException($payin->ExecutionDetails->SecureModeRedirectURL);
        }
        else {
          $payment->getPaymentMethod()->setReusable(false)->save();
          $payment->getOrder()->set('payment_method', NULL)->save();
          $payment->delete();

          \Drupal::logger('commerce_mangopay_dpi')->warning(sprintf('No SecureModeRedirectURL provided for Created response: %s: %s', $payin->ResultCode, $payin->ResultMessage));
          throw new PaymentGatewayException('Payment has not been processed correctly. Please try again with a different card.');
        }
      break;

      // Success, mark payment as completed and continue
      case \MangoPay\PayInStatus::Succeeded:
        $payment->setState('completed');
        $payment->setRemoteId($payin->Id);
        $payment->save();

      break;

      default:
        $payment->getPaymentMethod()->setReusable(false)->save();
        $payment->getOrder()->set('payment_method', NULL)->save();
        $payment->delete();
        
        \Drupal::logger('commerce_mangopay_dpi')->error(sprintf('Pay In Error: %s: %s', $payin->ResultCode, $payin->ResultMessage));
        throw new PaymentGatewayException('Payment has not been processed correctly. Please try again with a different card.');
      break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createNaturalUser($first_name, $last_name, $email, $dob, $nationality, $country, $address_line1 = '', $address_line2 = '', $city = '', $postal_code = '', $region = '', $occupation = '', $income_range = '', $tag = '') {
    $user = new \MangoPay\UserNatural();
    $user->FirstName = $first_name;
    $user->LastName = $last_name;
    $user->Email = $email;
    $user->CountryOfResidence = $country;
    $user->Nationality = $nationality;
    $user->Birthday = (int)$dob;
    $user->Occupation = $occupation;
    $user->IncomeRange = $income_range;

    if (!empty($address_line1)
    && !empty($city)
    && !empty($postal_code)) {
      $user->Address = new \MangoPay\Address();
      $user->Address->AddressLine1 = $address_line1;
      $user->Address->AddressLine2 = $address_line2;
      $user->Address->City = $city;
      $user->Address->PostalCode = $postal_code;
      $user->Address->Region = $region;
      $user->Address->Country = $country;
    }

    $user->Tag = $tag;
    return $this->api->Users->Create($user);
  }

  /**
   * {@inheritdoc}
   */
  public function getUser($user_id) {
    return $this->api->Users->Get($user_id);
  }

  /**
   * {@inheritdoc}
   */
  public function createWallet($user_id, $currency_code, $description, $tag = '') {
    $wallet = new \MangoPay\Wallet();
    $wallet->Owners = [$user_id];
    $wallet->Description = $description;
    $wallet->Currency = $currency_code;
    $wallet->Tag = $tag;
    return $this->api->Wallets->Create($wallet);
  }

  /**
   * {@inheritdoc}
   */
  public function getWallets($user_id) {
    return $this->api->Users->GetWallets($user_id);
  }

  /**
   * {@inheritdoc}
   */
  public function createCardRegistration($user_id, $currency_code, $card_type, $tag = '') {
    $cardRegister = new \MangoPay\CardRegistration();
    $cardRegister->UserId = $user_id;
    $cardRegister->Currency = $currency_code;
    $cardRegister->CardType = $card_type;
    $cardRegister->Tag = $tag;
    return $this->api->CardRegistrations->Create($cardRegister);
  }

  /**
   * {@inheritdoc}
   */
  public function createDirectPayIn($user_id, $wallet_id, $card_id, $amount, $currency_code, $secure_mode_return_url, $secure_mode = 'DEFAULT', $statement_descriptor = '') {

    // Create pay-in CARD DIRECT
    $pay_in = new \MangoPay\PayIn();
    $pay_in->CreditedWalletId = $wallet_id;
    $pay_in->AuthorId = $user_id;
    $pay_in->DebitedFunds = new \MangoPay\Money();
    $pay_in->DebitedFunds->Amount = $amount;
    $pay_in->DebitedFunds->Currency = $currency_code;
    $pay_in->Fees = new \MangoPay\Money();
    $pay_in->Fees->Amount = 0;
    $pay_in->Fees->Currency = $currency_code;
    $pay_in->StatementDescriptor = "";

    // Payment type as CARD
    // TODO: Do we have to make a call here? Why not storing this?
    // TODO: Shall we validate in case card no longer exists or is expired?
    $card = $this->api->Cards->Get($card_id);

    $pay_in->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
    $pay_in->PaymentDetails->CardType = $card->CardType;
    $pay_in->PaymentDetails->CardId = $card->Id;

    // Execution type as DIRECT
    $pay_in->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();

    // 3D Secure configuration
    $pay_in->ExecutionDetails->SecureMode = $secure_mode;
    $pay_in->ExecutionDetails->SecureModeReturnURL = $secure_mode_return_url;

    return $this->api->PayIns->Create($pay_in);
  }

  /**
   * {@inheritdoc}
   */
  public function getPayIn($payin_id) {
    return $this->api->PayIns->Get($payin_id);
  }


}
